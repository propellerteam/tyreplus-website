﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
public partial class Admin_DashBoard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session[GlobalVariables.SessionVariables.AdminLogin] != null)
            {
                BindGrid();
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (v_number.Text.Length > 0)
        {
            BindGrid();
        }
        else
        {
            // lblErrror.Text = "Pleaser Enter Voucher No !!!";
        }
    }
    public void BindGrid()
    {
        try
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string queryVoucherRedeem = "select count(vouchercode) as RedeemVoucher from [SubscribeUsers] where VoucherStatus=0";

            string queryTotalVoucher = "select count(isnull (vouchercode,'')) as TotalVoucher from [SubscribeUsers] where vouchercode is not null";

            DataSet sdrVoucherRedeem = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, queryVoucherRedeem);
            if (sdrVoucherRedeem.Tables.Count > 0 && sdrVoucherRedeem.Tables[0].Rows.Count > 0)
            {
                lblVoucherGenratedRedeem.Text = sdrVoucherRedeem.Tables[0].Rows[0]["RedeemVoucher"].ToString();
            }
            DataSet sdrVoucherGenrated = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, queryTotalVoucher);

            if (sdrVoucherGenrated.Tables.Count > 0 && sdrVoucherGenrated.Tables[0].Rows.Count > 0)
            {
                lblTotalVoucherGenrated.Text = sdrVoucherGenrated.Tables[0].Rows[0]["TotalVoucher"].ToString();
            }
        }
        catch (Exception e)
        {

        }
    }
    [WebMethod]
    public static string VoucherDetails(string voucherNo)
    {
        string result = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(voucherNo))
            {
                string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                string query = "select VoucherCode,(isnull(Firstname,'') +' ' +isnull(lastname,'')) as Name,convert(nvarchar(10), DateofRegistration,103) as RegDate,cast( VoucherType as nvarchar) as VoucherTypes,(select top 1 Branch from TPbranch where BranchID=SubscribeUsers.BranchID) as Branch ,VoucherStatus from SubscribeUsers where Firstname is not null";

                query += "   and VoucherCode like '%" + voucherNo + "%'";


                DataSet sdr = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, query);
                var outputAutocompleteData = sdr.Tables[0].AsEnumerable().Select(c =>

                    new
                    {
                        VoucherCode = c.Field<string>("VoucherCode"),
                        Name = c.Field<string>("Name"),
                        RegDate = c.Field<string>("RegDate"),
                        VoucherTypes = (c.Field<string>("VoucherTypes") == "1") ? "200 AED" : "100 AED",
                        Branch = c.Field<string>("Branch"),
                        VoucherStatus = (c.Field<string>("VoucherStatus") == "1") ? "Issued" : "Redeemed"
                    }).ToList();
                if (sdr.Tables.Count > 0 && sdr.Tables[0].Rows.Count > 0)
                {
                    //gridVoucherDetails.DataSource = sdr;
                    //gridVoucherDetails.DataBind();
                }

                JavaScriptSerializer serialiser = new JavaScriptSerializer();
                result = serialiser.Serialize(outputAutocompleteData);
            }
        }
        catch (Exception ex)
        {

        }
        return result;
    }

}