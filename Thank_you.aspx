﻿<%@ Page Title="TYREPLUS Quiz" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Thank_you.aspx.cs" Inherits="Thank_you" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="thank_you_content">
				<!-- main heading -->
				<p class="thank_you_sign"><%=ResourceFileManager.Get("ThankyouPage_thank_you_sign_1_You_are_awesome","You are awesome")%></p>
				<p class="thank_you_ub_sign">
				<%=ResourceFileManager.Get("ThankyouPage_thank_you_ub_sign_1_1","And we like to surprise awesome people with brilliant benefits.")%>	<br/>
				<%=ResourceFileManager.Get("ThankyouPage_thank_you_ub_sign_1_2","Visit your inbox to see what’s inside.")%>		
				</p>
				<p class="thank_you_mark small">
				<%=ResourceFileManager.Get("ThankyouPage_thank_you_mark small_1_1","Be kind and share this offer with your friends,")%>	<br/>
						<%=ResourceFileManager.Get("ThankyouPage_thank_you_mark small_1_2","but be quick as we only have so many vouchers to give away.")%>
				</p>

				<div class="tankyou_social">
					<div class="social_text"><%=ResourceFileManager.Get("ThankyouPage_social_text_1_1_share","Share")%></div>
					<ul class="social_icons">
						<li id="s_facebook" class="share s_facebook so_icon icon_f"><a></a></li>
						<li id="s_twitter" class="share s_twitter so_icon icon_t"><a></a></li>
					</ul>
				</div>
			</div>
</asp:Content>

