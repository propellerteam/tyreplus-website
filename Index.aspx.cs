﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
public partial class Admin_Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session[GlobalVariables.SessionVariables.Branch] == null)
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            
        }
    }
    [WebMethod]
    public static string VoucherDetails(string voucherNo)
    {
        string result = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(voucherNo))
            {
                string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                string query = "select top 1 * from SubscribeUsers  ";

                query += "   where VoucherCode = '" + voucherNo + "'";


                DataSet sdr = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, query);
               
                if (sdr.Tables.Count > 0 && sdr.Tables[0].Rows.Count > 0)
                {
                    result = "Valid";
                } 
                else
                {
                    result = "Voucher number invalid. Please try a diffrent number.";
                }
                //JavaScriptSerializer serialiser = new JavaScriptSerializer();
                //result = serialiser.Serialize(result);
            }
        }
        catch (Exception ex)
        {

        }
        return result;
    }
    [WebMethod]
    public static string RedeemVoucherDetails(string voucherNo,string serviceType)
    {
        string result = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(voucherNo) && !string.IsNullOrEmpty(serviceType) && HttpContext.Current.Session[GlobalVariables.SessionVariables.Branch] != null)
            {
                string branch = HttpContext.Current.Session[GlobalVariables.SessionVariables.Branch].ToString();
                string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                string query = "select top 1 * from SubscribeUsers  ";

                query += "   where VoucherCode = '" + voucherNo + "'";


                DataSet sdr = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, query);
              
                if (sdr.Tables.Count > 0 && sdr.Tables[0].Rows.Count > 0)
                {

                    string query1 = "SELECT top 1 BranchID FROM TPbranch where Branch ='"+branch+"'";
                    object branch_Id = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, query1);
                    string branchID = branch_Id.ToString();
                    var serviceOption =GlobalVariables.ServiceOption.FirstOrDefault(x=>x.Text==serviceType.Trim());
                    string VoucherType = (serviceOption!=null)?serviceOption.Value:string.Empty;
                    DateTime RedeemDate = System.DateTime.Now;
                    int  VoucherStatus =0;
                    //Insert Case
                    query = "update SubscribeUsers  Set BranchID=@BranchID,VoucherType=@VoucherType,VoucherStatus=@VoucherStatus,RedeemDate=@RedeemDate where [VoucherCode]=@VoucherCode and VoucherStatus=1;";
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter("@BranchID", branchID));
                    parameters.Add(new SqlParameter("@VoucherType", VoucherType));
                    parameters.Add(new SqlParameter("@VoucherStatus", VoucherStatus));
                   
                    parameters.Add(new SqlParameter("@RedeemDate", RedeemDate));
                    parameters.Add(new SqlParameter("@VoucherCode", voucherNo));
                    
                    int id = (int)SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, query, parameters.ToArray());

                    if (id > 0)
                    {
                        result = "Redeemed";
                    }
                    else
                    {
                        result = "Already Redeemed";
                    }
                    
                    //Send Email 
                   
                    
                }
            }
        }
        catch (Exception ex)
        {

        }
        return result;
    }
}