﻿<%@ Page Title="TYREPLUS Quiz" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Question.aspx.cs" Inherits="Question" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="css/contact.css">
	<link rel="stylesheet" href="css/form-rules.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
  
			<div class="question_progress">
				<p><%-- <%=ResourceFileManager.Get("QuestionPage_Question_1_5"," Question 1 / 5") %>--%>
                     <%=ResourceFileManager.Get("QuestionPage_Question_No"," Question ") %>
				</p>
				<div class="question_progress_bar">
					<div class="question_progress_bar_progress"></div>
				</div>
			</div>

			<!-- all questions -->
			<div class="questions_container">
				<!-- question 1 -->
				<div class="question_part visible"  id="question_1">
					<!-- question itself -->
					<div class="question_right_text"> <%=ResourceFileManager.Get("QuestionPage_Question_One"," Question One") %></div>

					<!-- question explanation -->
					<div class="question_explanation"> <%=ResourceFileManager.Get("QuestionPage_question_explanation_1","Are you aware about TYREPLUS car care network?") %></div>

					<!-- question variants -->
					<ul class="question_right_answers single_answer">
						<li>
							<input type="checkbox" class="change_dates" id="q1_ch1" name="q1_ch1" />
							<label for="q1_ch1"> <%=ResourceFileManager.Get("QuestionPage_question_right_answers_1_1_q1_ch1_Yes","Yes") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q1_ch2" name="q1_ch2" />
							<label for="q1_ch2"> <%=ResourceFileManager.Get("QuestionPage_question_right_answers_1_2_q1_ch2_No","No") %></label>
						</li>
					</ul>
				</div>

				<!-- question 2 -->
				<div class="question_part question_sign"  id="question_2">
					<!-- question itself -->
					<div class="question_right_text"> <%=ResourceFileManager.Get("QuestionPage_question_right_text_1_Question_Two","Question Two") %> </div>

					<!-- question explanation -->
					<div class="question_explanation"> <%=ResourceFileManager.Get("QuestionPage_question_explanation_2_1_Challenge_your_knowledge","Challenge your knowledge.") %>
						 <br/>
					 <%=ResourceFileManager.Get("QuestionPage_question_explanation_2_2_Click_the_four_services","Click the  four services") %>
					 <%=ResourceFileManager.Get("QuestionPage_question_explanation_2_3_	provided_by_TYREPLUS.","provided by TYREPLUS.") %>
					</div>

					<!-- question help explanation -->
					<div class="question_explanation_small">
						<span class="more_naswers">	 <%=ResourceFileManager.Get("QuestionPage_question_explanation_2_1_small_Select_at_least_4_answers","Select at least 4 answers") %></span>
						<span class="less_naswers"> <%=ResourceFileManager.Get("QuestionPage_question_explanation_2_2_small_Select_4_answers_maximum","Select 4 answers maximum") %></span>
					</div>

					<!-- question variants -->
					<ul class="question_right_answers four_answers">
						<li>
							<input type="checkbox" class="change_dates" id="q2_ch1" name="q2_ch1" />
							<label for="q2_ch1"> <%=ResourceFileManager.Get("QuestionPage_question_right_answers_2_1_q2_ch1_Oil_change","Oil change") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q2_ch2" name="q2_ch2" />
							<label for="q2_ch2"> <%=ResourceFileManager.Get("QuestionPage_question_right_answers_2_2_q2_ch2_AC_Gas_Refill","AC Gas Refill") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q2_ch3" name="q2_ch3" />
							<label for="q2_ch3"> <%=ResourceFileManager.Get("QuestionPage_question_right_answers_2_3_q2_ch3_ Window_Tinting","Window_Tinting") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q2_ch4" name="q2_ch4" />
							<label for="q2_ch4"> <%=ResourceFileManager.Get("QuestionPage_question_right_answers_2_4_q2_ch4_Brakes","Brakes") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q2_ch5" name="q2_ch5" />
							<label for="q2_ch5"><%=ResourceFileManager.Get("QuestionPage_question_right_answers_2_5_q2_ch5_Batteries","Batteries") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q2_ch6" name="q2_ch6" />
							<label for="q2_ch6"><%=ResourceFileManager.Get("QuestionPage_question_right_answers_2_6_q2_ch6_Light_change","Light change") %></label>
						</li>
					</ul>
				</div>

				<!-- question 3 -->
				<div class="question_part question_sign"  id="question_3">
					<!-- question itself -->
					<div class="question_right_text"><%=ResourceFileManager.Get("QuestionPage_question_right_text_2_Question_Three","Question Three") %></div>

					<!-- question explanation -->
					<div class="question_explanation">
					<%=ResourceFileManager.Get("QuestionPage_question_explanation-3_change_your_tyres?","How often do you think you should change your tyres?") %>	
					</div>

					<!-- question help explanation -->
					<div class="question_explanation_small"><%=ResourceFileManager.Get("QuestionPage_question_explanation_3_small_1_answer","1 answer") %></div>

					<!-- question variants -->
					<ul class="question_right_answers single_answer">
						<li>
							<input type="checkbox" class="change_dates" id="q3_ch1" name="q3_ch1" />
							<label for="q3_ch1"><%=ResourceFileManager.Get("QuestionPage_question_right_answers_3_1_q3_ch1_Every_Six_Months","Every Six Months") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q3_ch2" name="q3_ch2" />
							<label for="q3_ch2"><%=ResourceFileManager.Get("QuestionPage_question_right_answers_3_2_q3_ch2_Once_a_Year","Once a Year") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q3_ch3" name="q3_ch3" />
							<label for="q3_ch3"><%=ResourceFileManager.Get("QuestionPage_question_right_answers_3_3_q3_ch3_Every_Two_Years","Every Two Years") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q3_ch4" name="q3_ch4" />
							<label for="q3_ch4"><%=ResourceFileManager.Get("QuestionPage_question_right_answers_3_4_q3_ch4_Every_Five_Years","Every Five Years") %></label>
						</li>
					</ul>
				</div>

				<!-- question 4 -->
				<div class="question_part question_sign label_images"  id="question_4">
					<!-- question itself -->
					<div class="question_right_text"><%=ResourceFileManager.Get("QuestionPage_question_right_text_3_Question_Four","Question Four") %></div>

					<!-- question explanation -->
					<div class="question_explanation">
						<%=ResourceFileManager.Get("QuestionPage_question_explanation_4_warning_sign_to_show_your_breaks_require_changing","Select the correct warning sign to show your batteries require service?") %>
					</div>

					<!-- question help explanation -->
					<div class="question_explanation_small"><%=ResourceFileManager.Get("QuestionPage_question_explanation_4_small_1_answer","1 answer") %></div>

					<!-- question variants -->
					<ul class="question_right_answers single_answer">
						<li class="q4_1">
							<input type="checkbox" class="change_dates" id="q4_ch1" name="q4_ch1" />
							<label for="q4_ch1">
								<img src="img/check_arrow.png">
							</label>
						</li>
						<li class="q4_2">
							<input type="checkbox" class="change_dates" id="q4_ch2" name="q4_ch2" />
							<label for="q4_ch2">
								<img src="img/check_arrow.png">
							</label>
						</li>
					</ul>
				</div>

				<!-- question 5 -->
				<div class="question_part question_sign"  id="question_5">
					<!-- question itself -->
					<div class="question_right_text"><%=ResourceFileManager.Get("QuestionPage_question_right_text_3_Question_Five","Question Five") %></div>

					<!-- question explanation -->
					<div class="question_explanation"><%=ResourceFileManager.Get("QuestionPage_question_explanation_5_1_How_often_do_you_change","How often do you change") %>
						
					<%=ResourceFileManager.Get("QuestionPage_question_explanation_5_2_the_oil_for_your_vehicle?","the oil for your vehicle?") %>	
					</div>

					<!-- question help explanation -->
					<div class="question_explanation_small"><%=ResourceFileManager.Get("QuestionPage_question_explanation_5_small_1_answer","1 answer") %></div>

					<!-- question variants -->
					<ul class="question_right_answers single_answer">
						<li>
							<input type="checkbox" class="change_dates" id="q5_ch1" name="q5_ch1" />
							<label for="q5_ch1"><%=ResourceFileManager.Get("QuestionPage_question_right_answers_4_1_Every_5,000_KMs","Every 5,000 KMs") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q5_ch2" name="q5_ch2" />
							<label for="q5_ch2"><%=ResourceFileManager.Get("QuestionPage_question_right_answers_4_2_Every_10,000_KMs","Every 10,000 KMs") %></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q5_ch3" name="q5_ch3" />
							<label for="q5_ch3"><%=ResourceFileManager.Get("QuestionPage_question_right_answers_4_3_Every_15,000_KMs","Every 15,000 KMs")%></label>
						</li>
						<li>
							<input type="checkbox" class="change_dates" id="q5_ch4" name="q5_ch4" />
							<label for="q5_ch4"><%=ResourceFileManager.Get("QuestionPage_question_right_answers_4_4_Other","Other") %></label>
						</li>
					</ul>
				</div>
			</div>
			<!-- all questions end-->

			<!-- analys part -->
			<section class="top_margin analys_container">
				<div class="analys_content">
					<img src="img/spinner.png" alt="" class="spinner rotation">
					<!-- main heading -->
					<p class="analyzing_sign"><%=ResourceFileManager.Get("analyzing_sign_1_Analyzing","Analyzing") %></p>
					<p class="analyzing_sub_sign"><%=ResourceFileManager.Get("analyzing_sub_sign_1_1_We are currently analyzing","We_are_currently_analyzing") %><br/>
                  <%=ResourceFileManager.Get("analyzing_sub_sign_1_2_your answers.","your answers.")%></p>
				</div>
			</section>
			<!-- analys part end-->

			<!-- contact form start -->
			<section class="form_container top_margin">
				<div class="claim_form">	
					<div class="text_part">
						<p class="text_heading"><%=ResourceFileManager.Get("QuestionPage_text_heading_1_Thank you_for_TYREPLUS_Quiz. ","Thank you for taking part in the TYREPLUS Quiz.") %></p>
						<p class="big_white_text">
							<%=ResourceFileManager.Get("QuestionPage_big_white_text_1_Please_fill_in_your_details","Please fill in your details to the right and you will receive a voucher for any of the below:") %>
						</p>
						<ul class="vouchers">
							<li>
								<p><%=ResourceFileManager.Get("QuestionPage_vouchers_1_200 AED_off_your_Michelin-tyre_purchase","200 AED off your Michelin tyre purchase") %>*</p>
							</li>
							<li>
								<p><%=ResourceFileManager.Get("QuestionPage_vouchers_2_100 AED off on services","100 AED off on services") %></p>
							</li>
						</ul>
		 				<p class="small_white_text">
		 					<%=ResourceFileManager.Get("QuestionPage_small_white_text_1_TYREPLUS_is_Michelins_premium_car_care_network_in_the_Gulf","TYREPLUS is Michelins premium car care network in the Gulf, with a total of 31 branches across the UAE alone.") %>
		 				</p>
		 				<a class="tc_small_text" href="terms.apsx"> 
		 					<%=ResourceFileManager.Get("QuestionPage_small_white_text_1_TYREPLUS_is_Michelins_premium_car_care_network_in_the_Gulf_terms_and_conditions","* Terms &amp; Conditions apply") %>
		 				</a>
					</div>		

					<div class="form_part">
						<p class="text_heading"><%=ResourceFileManager.Get("QuestionPage_text_heading_2_Fill_in_your_details","Fill in your details") %> </p>
						<p class="error_message">
							<span class="error1">
								<%=ResourceFileManager.Get("Form_error_1","Please enter your first name") %>
							</span>
							<span class="error2">
								<%=ResourceFileManager.Get("Form_error_2","Please enter your last name") %>
							</span>
							<span class="error3">
								<%=ResourceFileManager.Get("Form_error_3","Please enter a valid email") %>
							</span>
							<span class="error4">
								<%=ResourceFileManager.Get("Form_error_4","Please enter a valid mobile number") %>
							</span>
							<span class="error5">
								<%=ResourceFileManager.Get("Form_error_5","Please accept the Terms and Conditions") %>
							</span>
                            <span class="error6">
								<%=ResourceFileManager.Get("Form_error_6","Email address already exists.") %>
							</span>
                            <span class="error7">
								<%=ResourceFileManager.Get("Form_error_7","Please enter the city.") %>
							</span>
						</p>
						<div class="input_group">
							<div class="material_input">
								<label for="u_name"><%=ResourceFileManager.Get("QuestionPage_material_input_1_First_name","First name") %></label>
								<input type="text" name="u_name" id="u_name">
							</div>
							<div class="material_input">
								<label for="u_last_name"><%=ResourceFileManager.Get("QuestionPage_material_input_2_Last_name","Last name") %></label>
								<input type="text" name="u_last_name" id="u_last_name">
							</div>
						</div>
						<div class="input_group">
							<div class="material_input">
								<label for="email"><%=ResourceFileManager.Get("QuestionPage_material_input_3_Email","Email") %></label>
								<input type="text" name="email" id="email">
							</div>
							<div class="material_input">
								<label for="mobile"><%=ResourceFileManager.Get("QuestionPage_material_input_4_Mobile_Number","Mobile Number") %></label>
								<input type="text" name="mobile" id="mobile">
							</div>
						</div>

						<div class="input_group_full_width">
							<div class="custom_dropdown ">
								<!-- label with displayed value -->
								<div class="dd_label focused">
									<div class="dd_label_value"><%=ResourceFileManager.Get("QuestionPage_dd_options_1_1_Abu_Dhabi","Abu Dhabi") %></div>
									<div class="dd_label_sign"><%=ResourceFileManager.Get("QuestionPage_dd_label_sign_1_City","City") %></div>
								</div>

								<!-- possible options -->
								<ul class="dd_options">
									<li data-id="1"><%=ResourceFileManager.Get("QuestionPage_dd_options_1_1_Abu_Dhabi","Abu Dhabi") %></li>
									<li data-id="2"><%=ResourceFileManager.Get("QuestionPage_dd_options_1_2_Ajman","Ajman") %></li>
									<li data-id="3"><%=ResourceFileManager.Get("QuestionPage_dd_options_1_3_Dubai","Dubai") %></li>
									<li data-id="4"><%=ResourceFileManager.Get("QuestionPage_dd_options_1_4_Fujairah","Fujairah") %></li>
									<li data-id="5"><%=ResourceFileManager.Get("QuestionPage_dd_options_1_5_Ras_Al_Khaimah","Ras Al Khaimah") %></li>
									<li data-id="6"><%=ResourceFileManager.Get("QuestionPage_dd_options_1_6_Sharjah","Sharjah") %></li>
									<li data-id="7"><%=ResourceFileManager.Get("QuestionPage_dd_options_1_7_Umm_Al_Quwain","Umm Al Quwain") %></li>
								</ul>

								<!-- binded input -->
								<input id="city" name="city" type="text">
							</div>
						</div>

						<div class="input_group">
							<div class="checkboxes">
								<div class="checkbox_holder">
									<input type="checkbox" class="change_dates" id="ch1" name="ch1" />
									<label for="ch1"><span></span><p><%=ResourceFileManager.Get("QuestionPage_checkbox_holder_1_1_Accept","Accept") %>
                                         <a href="Terms.aspx" target="_blank"><%=ResourceFileManager.Get("QuestionPage_checkbox_holder_1_2_Terms","Terms") %> &amp;<%=ResourceFileManager.Get("QuestionPage_checkbox_holder_3_Conditions","Conditions") %></a></p></label>
								</div>
								<div class="checkbox_holder">
									<input type="checkbox" class="change_dates" id="ch2" name="ch2" />
									<label for="ch2"><span></span><p><%=ResourceFileManager.Get("QuestionPage_checkbox_holder_2_1_about_our_future_offers","Please tick if you'd like to hear about our future offers") %></a></p></label>
								</div>
							</div>
							
							<a class="submit_btn" id="submit_btn"><%=ResourceFileManager.Get("QuestionPage_submit_btn_1_Claim_your_voucher","Claim your voucher") %></a>
						</div>
					</div>
			</section>
			<!-- contact form end -->

			<!-- buttons for question navigation -->
			<div class="buttons_container hidden">
				<div class="first_no_answer">
					<p><%=ResourceFileManager.Get("QuestionPage_first_no_answer","TYREPLUS is Michelin's car care network specialized in selling premium tyre brands and providing all related service and car maintenance such as oil change, AC gas refill, brake and batteries.") %></p>
					<img src="img/michlen.png" alt="">
				</div>
                
				<a id="next_button" class="next_button" data-answered=""> <%=ResourceFileManager.Get("QuestionPage_NextButton","Next") %></a>
			</div>


    		<script>
    		</script>

   



	</asp:Content>


