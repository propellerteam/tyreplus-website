﻿<%@ Page Title="Index : TYREPLUS Quiz" Language="C#" AutoEventWireup="true"  MasterPageFile="~/Admin/AdminMaster.master" CodeFile="DashBoard.aspx.cs" Inherits="Admin_DashBoard" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <%-- <div>
        <asp:Label ID="lblErrror" runat="server" ForeColor="Red"></asp:Label>
        <br />
        <h3>No. of Voucher redeem : </h3>
        <br />
        <h3>Total No. of Voucher genrated :</h3>
        <br />
        <div>
        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            <asp:Button ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_Click"/>
        </div>
        <asp:GridView runat="server" ID="gridVoucherDetails"></asp:GridView>
    </div>--%>


    <div class="form">
					<div class="form_heading">Total Vouchers:</div>

					<div class="vouchers_info">
						<div class="info_descr issued">
							<p class="text">Issued</p>
							<p class="number"><asp:Label ID="lblTotalVoucherGenrated" runat="server" ></asp:Label></p>
						</div>
						<div class="info_descr redeemed">
							<p class="text">Redeemed</p>
							<p class="number"> <asp:Label ID="lblVoucherGenratedRedeem" runat="server" ></asp:Label></p>
						</div>
					</div>

					<div class="input_group_full_width">
						<div class="custom_dropdown datalist">
							<!-- label with displayed value -->
							<div class="dd_label">
								<!-- binded input -->
								<%--<input id="v_number" name="v_number" type="text">--%>
                                <asp:TextBox ID="v_number" ClientIDMode="Static" runat="server"></asp:TextBox>
								<div class="dd_label_sign">Search by voucher number</div>
							</div>

							<!-- possible options -->
							<%--<ul class="dd_options">
								<li>ABC 1234567890</li>
								<li>ABC 1234567888</li>
								<li>ABC 1234444444</li>
								<li>ABC 1235555555</li>
							</ul>--%>
            </div>
        </div>

    </div>
   
</asp:Content>

