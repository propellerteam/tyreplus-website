//npm install gulp gulp gulp-watch gulp-stylus gulp-autoprefixer browser-sync gulp-plumber --save-dev

// vars
var gulp = require('gulp');
var watch = require('gulp-watch');
var stylus = require('gulp-stylus');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var plumber = require('gulp-plumber');
var reload = browserSync.reload;

var path = {
		src:{
			html: './src/*.html',
			html_template: './src/template/**/*.html',
			html_admin: './src/admin/**/*.html',
			js: './src/js/**/*.js',
			css: './src/css/**/*.styl',
			css_raw: './src/css/**/*.css',
			img: './src/img/*.*',
			img_temp: './src/img/*/*.*',
			fonts: './src/fonts/*.*'
		},
		build:{
			html: './build/',
			html_template: './build/template/',
			html_admin: './build/admin/',
			js: './build/js/',
			css: './build/css/',
			img: './build/img/',
			img_temp: './build/img/',
			fonts: './build/fonts/'
		}
	}

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 8005,
    logPrefix: "Master Andrew"
};

// assisting functions
function handleError(e){
	console.log(e.toString());
	this.emit('end');
}

// tasks
gulp.task('html', function(){
	return gulp.src(path.src.html)
		.pipe(plumber())
		.pipe(gulp.dest(path.build.html))
		.on('error', handleError)
		.pipe(reload({stream: true}))
});

gulp.task('html_template', function(){
	return gulp.src(path.src.html_template)
		.pipe(plumber())
		.pipe(gulp.dest(path.build.html_template))
		.on('error', handleError)
		.pipe(reload({stream: true}))
});

gulp.task('html_admin', function(){
	return gulp.src(path.src.html_admin)
		.pipe(plumber())
		.pipe(gulp.dest(path.build.html_admin))
		.on('error', handleError)
		.pipe(reload({stream: true}))
});

gulp.task('js', function(){
	return gulp.src(path.src.js)
		.pipe(plumber())
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}))
});

gulp.task('style', function(){
	return gulp.src(path.src.css)
		.pipe(plumber())
		.pipe(stylus())
		.pipe(autoprefixer())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream: true}))
});

gulp.task('style_raw', function(){
	return gulp.src(path.src.css_raw)
		.pipe(autoprefixer())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream: true}))
});

gulp.task('img', function(){
	return gulp.src(path.src.img)
		.pipe(plumber())
		.pipe(gulp.dest(path.build.img))
		.pipe(reload({stream: true}))
});
gulp.task('img_temp', function(){
	return gulp.src(path.src.img_temp)
		.pipe(plumber())
		.pipe(gulp.dest(path.build.img_temp))
		.pipe(reload({stream: true}))
});

gulp.task('fonts', function(){
	return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
		.pipe(reload({stream: true}))
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('watch', function(){
	watch([path.src.html, path.src.html_admin], function(){
		gulp.start('html');
		gulp.start('html_admin');
	});
	watch([path.src.js], function(){
		gulp.start('js');
	});
	watch([path.src.css], function(){
		gulp.start('style');
	});
	watch([path.src.css_raw], function(){
		gulp.start('style_raw');
	});
	watch([path.src.img], function(){
		gulp.start('img');
	});
	watch([path.src.img_temp], function(){
		gulp.start('img_temp');
	});
	watch([path.src.fonts], function(){
		gulp.start('fonts');
	});

});

gulp.task('build', ['html', 'html_admin', 'js', 'style', 'style_raw', 'img', 'img_temp', 'fonts']);

gulp.task('default', ['build', 'webserver', 'watch']);