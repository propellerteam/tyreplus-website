﻿<%@ Page Title="Index : TYREPLUS Quiz" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Admin_Index" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="form">
        <div class="form_heading">Enter voucher details</div>

        <div class="material_input">
            <label for="v_number">Enter a voucher number</label>
            <%--		<input type="text" name="v_number" id="v_number">--%>
            <asp:TextBox ID="vnumber" ClientIDMode="Static" runat="server"></asp:TextBox>
        </div>

        <div class="error_messages">
            <div class="voucher_redeemed">This voucher has been redeemed. Please try a different number.</div>
            <div class="voucher_invalid">Voucher number invalid. Please try a different number.</div>
        </div>

        <div class="input_group_full_width">
            <div class="custom_dropdown">
                <!-- label with displayed value -->
                <div class="dd_label">
                    <div class="dd_label_value"></div>
                    <div class="dd_label_sign">Select service option</div>
                </div>

                <!-- possible options -->
                <ul class="dd_options">
                    <% foreach (ListItem item in GlobalVariables.ServiceOption)
                       {%>
                    <li <%=item.Value %>><%=item.Text %></li>
                    <%  } %>
                </ul>

                <!-- binded input -->
                <input style="position: absolute; z-index: -100; opacity: 0; left: -10000" id="city" name="city" type="text" />
            </div>
        </div>

        <a class="submit_btn" id="Submit_RedeemVoucher">Redeem Voucher</a>
    </div>
</asp:Content>

