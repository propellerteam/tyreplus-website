﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SiteMaster : MasterPage
{

    protected void Page_Load(object sender, EventArgs e)
    {   // added by Andrew
        var pageName = HttpContext.Current.Request.Url.AbsolutePath.ToString().ToLower();

        var url = "<meta property=\"og:url\" content=\"http://tyreplusquiz.com/\" />";
        var img = "<meta property=\"og:image\" content=\"http://tyreplusquiz.com/img/fb.jpg\" />";
        var title = "<meta property=\"og:title\" content=\"TYREPLUS Quiz\" />";
        var desc = "<meta property=\"og:description\" content=\"I just took the TYREPLUS quiz and won a great discount for my next visit to any of their 36 UAE branches. Check it out!\" />";
            
        var tw_card = "<meta property=\"twitter:card\" content=\"summary_large_image\" />";
        var tw_title = "<meta property=\"twitter:title\" content=\"Parade of Fans for Houston’s Funeral\" />";
        var tw_img = "<meta property=\"twitter:image\" content=\"http://tyreplusquiz.com/img/fb.jpg\" />";
        var tw_desc = "<meta property=\"twitter:description\" content=\"NEWARK - The guest list and parade of limousines with celebrities emerging from them seemed more suited to a red carpet event in Hollywood or New York than than a gritty stretch of Sussex Avenue near the former site of the James M. Baxter Terrace public housing project here.\" />";
        litMeta.Text = img + title + desc + tw_card + tw_title + tw_img + tw_desc;
        // added by Andrew

        var language = GlobalUtilitiesOperations.GetCookie(GlobalVariables.CookieVariables.LanguageCookie);
        if (string.IsNullOrEmpty(language))
        {
            GlobalUtilitiesOperations.SetCookie(GlobalVariables.CookieVariables.LanguageCookie, GlobalVariables.DefaultLanguage);
            language = GlobalVariables.DefaultLanguage;
        }
        var requiredclass = " content_container content_container_landing ";
        if (HttpContext.Current.Request.Url.AbsolutePath.ToString().ToLower() == "/question.aspx" || HttpContext.Current.Request.Url.AbsolutePath.ToString().ToLower() == "/thank_you.aspx")
        {
            requiredclass = requiredclass + "  blured_bg";
        }
        else if (HttpContext.Current.Request.Url.AbsolutePath.ToString().ToLower() == "/default.aspx")
        {
            requiredclass = requiredclass + "  no_bg";
            mainbody.Attributes.Add("data-vide-bg", "img/mov");
        }// added by Andrew
        else if (pageName == "/terms.aspx" ||
                 pageName == "/about.aspx" ||
                 pageName == "/contact.aspx" ||
                 pageName == "/pivacy.aspx")
        {
            requiredclass = requiredclass + "  small_bg";
        }// added by Andrew

        mainform.Attributes.Add("class", requiredclass);

        mainbody.Attributes.Add("class", "lang_" + language.ToLower());
    }
    protected void langEnglish_Click(object sender, EventArgs e)
    {
        GlobalUtilitiesOperations.SetCookie(GlobalVariables.CookieVariables.LanguageCookie, GlobalVariables.DefaultLanguage);
        Response.Redirect(Request.Url.ToString(),true);
    }
    protected void langArabic_Click(object sender, EventArgs e)
    {
        GlobalUtilitiesOperations.SetCookie(GlobalVariables.CookieVariables.LanguageCookie, GlobalVariables.ArabicLanguage);
        Response.Redirect(Request.Url.ToString(), true);
    }
}