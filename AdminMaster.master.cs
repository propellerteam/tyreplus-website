﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AdminMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //if (session[globalvariables.sessionvariables.adminlogin] == null || session[globalvariables.sessionvariables.branch] ==null)
            //{
            //    response.redirect("~/admin/default.aspx");
            //}           
        }
    }

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        Session[GlobalVariables.SessionVariables.AdminLogin] = null;
        Session[GlobalVariables.SessionVariables.Branch] = null;
        Response.Redirect("~/Admin/Default.aspx");
    }
}
