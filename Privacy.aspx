﻿<%@ Page Title="TYREPLUS Quiz" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Privacy.aspx.cs" Inherits="Privacy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="css/about.css">
	<link rel="stylesheet" href="css/terms.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="page_content left_alignment">
				<div class="grid_container">
					<h1 class="about_heading"><%=ResourceFileManager.Get("PrivacyPage_about_heading_1_1_Privacy_policy","Privacy policy")%></h1>
					<ul class="legal_notice">
						<li><%=ResourceFileManager.Get("PrivacyPage_legal_notice_1_1_Legal_Notice","Legal Notice")%></li>
						<li><%=ResourceFileManager.Get("PrivacyPage_legal_notice_1_2_This_Site_is_published_by:","This Site is published by:")%></li>
						<li><%=ResourceFileManager.Get("PrivacyPage_legal_notice_1_3Michelin AIM FZE","Michelin AIM FZE")%></li>
						<li><%=ResourceFileManager.Get("PrivacyPage_legal_notice_1_4","Jebel Ali Free Zone")%></li>
						<li><%=ResourceFileManager.Get("PrivacyPage_legal_notice_1_5","PO Box: 263 034")%></li>
					</ul>

					<p class="text_body">
				<%=ResourceFileManager.Get("PrivacyPage_text_body_1_1_Dubai","Dubai, UNITED ARAB EMIRATES")%>		
                        <br/><%=ResourceFileManager.Get("PrivacyPage_text_body_1_2"," Please read this legal notice carefully before consulting this Site.")%>	
                        <br/> <%=ResourceFileManager.Get("PrivacyPage_text_body_1_3","The companies in the Michelin Group have their own corporate existence and an autonomous corporate personality. However, to simplify communication of the information presented on this Site, the following terms may be used:")%>	
                         "<%=ResourceFileManager.Get("PrivacyPage_text_body_1_4_Michelin","Michelin")%>", 
                        "<%=ResourceFileManager.Get("PrivacyPage_text_body_1_5_Group","Group")%>", 
                        "<%=ResourceFileManager.Get("PrivacyPage_text_body_1_6_Michelin_Group","Michelin Group")%>"; 
                      <%=ResourceFileManager.Get("PrivacyPage_text_body_1_7_and","and")%>
                           "<%=ResourceFileManager.Get("PrivacyPage_text_body_1_8","we")%>"; 
                      <%=ResourceFileManager.Get("PrivacyPage_text_body_1_9"," these terms are used to refer to all the companies in the Group, each of which does business independently.")%>
					</p>
					<p class="text_body">
						<span class="text_heading"><%=ResourceFileManager.Get("PrivacyPage_text_heading_1_1_ARTICLE_1","ARTICLE 1: PURPOSE")%>	</span>
					<%=ResourceFileManager.Get("PrivacyPage_text_body_2_1","The purpose of the present notice is to define the conditions in which Michelin is making its site available and the conditions under which you may access and use the site, subject to compliance with the present legal notice which Michelin reserves the right to modify or update at any time. Access to and use of this site imply your agreement with the present legal notice and with Michelin’s practices concerning personal data. If you do not agree with the clauses below, you are advised not to use this Site.")%>		
					</p>

					<p class="text_body">
						<span class="text_heading"><%=ResourceFileManager.Get("PrivacyPage_text_heading_2_1_ARTICLE_2","ARTICLE 2: ACCESS TO THE SITE")%>	</span>
					<%=ResourceFileManager.Get("PrivacyPage_text_body_3_1","Michelin strives to keep the Site accessible, without however being subject to any obligation to achieve this. It is specified that access to the Site may be interrupted for the purposes of maintenance, updating or any other reason, particularly of a technical or legal nature. Under no circumstances shall Michelin be liable for these interruptions and the consequences which could result from them for the user.<br/> You agree not to access the site, and in particular any interactive or commercial services, fraudulently.")%>		
					</p>
					
					<p class="text_body">
						<span class="text_heading"><%=ResourceFileManager.Get("PrivacyPage_text_heading_3_1_ARTICLE_3","ARTICLE 3: COPYRIGHT - INTELLECTUAL PROPERTY")%>	</span>
						<%=ResourceFileManager.Get("PrivacyPage_text_body_4_1","The contents (notably data, soundtracks, information, illustrations, logos, brands, etc., as well as the format and appearance of the site) which appear or are available on this site are protected by copyright and other intellectual property rights and are the exclusive property of their respective publishers. No part of the contents of the MICHELIN site in general, even if it belongs to a third party, may be copied, reproduced, represented, adapted, altered, modified or circulated fully or partially by any means whatever, with the exception of a single copy, stored on a single computer and reserved exclusively for the private use of the copier. The elements presented in this site are liable to modification without notice and are presented without any express or tacit guarantee and are not considered as giving any rights of compensation. The information and images contained in the Internet site are protected by copyright 1997-2012 MICHELIN or covered by its partners' copyright. The logos are registered trade marks.")%>	
					</p>
					
					<p class="text_body">
						<span class="text_heading"><%=ResourceFileManager.Get("PrivacyPage_text_heading_4_1_ARTICLE_4","ARTICLE 4: USE OF THE DOCUMENTS")%>	</span>
					<%=ResourceFileManager.Get("PrivacyPage_text_body_5_1","None of the documents from this site can be copied, reproduced, published, downloaded, posted, transmitted or distributed in any manner, except for the case of a simple recording of documents on your personal computer for your own use and without any commercial aim. In this case, you should ensure that the indications of ownership are kept intact. Alteration and modification of these documents or use of them for another purpose constitutes an infringement of the property rights of Michelin or of a third party.")%>		
					</p>
					
					<p class="text_body">
						<span class="text_heading"><%=ResourceFileManager.Get("PrivacyPage_text_heading_5_1_ARTICLE_5","ARTICLE 5: E-MAIL")%>	</span>
						<%=ResourceFileManager.Get("PrivacyPage_text_body_6_1","To correspond with Michelin by e-mail, internet users must fill in the electronic correspondence forms proposed on the site.")%>	
					</p>
					
					<p class="text_body">
						<span class="text_heading"><%=ResourceFileManager.Get("PrivacyPage_text_heading_6_1_ARTICLE_6","ARTICLE 6: LIMITATION OF LIABILITY")%>	</span>
					<%=ResourceFileManager.Get("PrivacyPage_text_body_7_1","The documents and information supplied on this site are provided 'as they stand', without any express or tacit guarantee of any kind. Michelin reserves the right to modify or correct the site's contents at any time, without notice. Michelin cannot be held responsible in the event of contamination of users' computer equipment as a result of the propagation of a virus or other computer 'infections'. The site user is responsible for taking all appropriate measures to protect the user's own data and/or software from any possible contamination by computer viruses circulating via Internet. Michelin, its employees, suppliers or the partners mentioned on the Michelin site are not responsible in any case for any claims whatsoever, whether involving contractual responsibility, criminal responsibility or otherwise or for any direct, indirect incidental or consequential damage of whatever nature or for any prejudice, particularly of a financial or commercial nature, resulting from the use of this site or any item of information obtained from this site.")%>		
					<%=ResourceFileManager.Get("PrivacyPage_text_body_8_1","The information, particularly of a financial nature, provided on the Site cannot be considered an inducement to invest or to enter into transactions of any form whatsoever involving Michelin shares. Under no circumstances should such information be interpreted as a solicitation or distribution of shares to the public and it does not constitute an offer involving the purchase, sale or exchange of shares or other Michelin securities, listed or otherwise.")%>		
					<%=ResourceFileManager.Get("PrivacyPage_text_body_9_1","This Site may contain simple or complex links to other Michelin or third-party partner sites. Michelin has no control over these sites and therefore cannot be held responsible for the availability of these sites, their content, advertising material, products, and/or services available at or through these sites. Thus, Michelin shall not be liable in any way for any direct, indirect, incidental or consequential damage that may occur when the user accesses or uses these sites and it shall not be liable for any non-compliance of the contents and services with any regulations or for any information that infringes the rights of a third party. Michelin disclaims any responsibility concerning the information, materials and software on the sites linked by hypertext links to this Site. </br> Certain information presented on this site is supplied by information providers outside Michelin. The aforementioned content providers have exclusive liability regarding the contents and services provided by them on this site. Consequently, Michelin shall not be held liable for errors, interruptions or delays in the transmission of information, or for their consequences.")%>	
					</p>
					
					<p class="text_body">
						<span class="text_heading"><%=ResourceFileManager.Get("PrivacyPage_text_heading_7_1_ARTICLE_7","ARTICLE 7: PROTECTION OF PERSONAL DATA")%></span>
					<%=ResourceFileManager.Get("PrivacyPage_text_body_10_1","Generally, when you browse this site you do not have to provide identification or any personal data. However, we may sometimes ask you for such information, for example, in order to respond to a question from you, to provide services, or for marketing purposes.We, and third parties acting on our behalf or selected by us, may use the information you give to us.")%>	
							
                        <br/><%=ResourceFileManager.Get("PrivacyPage_text_body_11_1","In order to improve service to you, we collect anonymous data about navigation on this site (pages visited, date and time of visit, etc.). We may use this data to analyse usage patterns or for statistical measurements and studies. We also use “cookies” to improve and personalize the performance of the site. You can refuse to accept cookies; however, if you do so certain parts of the site may not be accessible to you.In the event that you do give us personal data, you have the right to access such data and request that it be corrected or suppressed. All requests concerning your personal data should be directed to: e-mail Michelin here.<br/>This site may contain links to other sites. We do not share your personal data with such sites. However, when you leave this site via a link to another site, your activities and personal data on such other site are subject to the practices of the provider of the other site, over which we have no control.")%>
						
					</p>
					
					<p class="text_body">
						<span class="text_heading"><%=ResourceFileManager.Get("PrivacyPage_text_heading_8_1_ARTICLE_8","ARTICLE 8: LEGAL JURISDICTION AND APPLICABLE LAW")%> </span>
					<%=ResourceFileManager.Get("PrivacyPage_text_body_12_1","Any disputes relating to the Site or to the present legal notice shall be brought before the courts in Dubai, the United Arab Emirates, and will be governed and analysed according to the law of Dubai, United Arab Emirates. Use of this site indicates your express agreement with the application of the present jurisdiction clause. In the event of one of the elements of these conditions of use being judged illegal, null or inapplicable for whatever reason, it will no longer be considered part of the conditions of use and will not affect the validity or application of the other elements.")%>	
					</p>
				</div>
			</div>


</asp:Content>

