﻿using Landing.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;


using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

public partial class Question : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
         //SaveTextOnImage(Server.MapPath("~/Image/image.png"), Server.MapPath("~/Image/"+Guid.NewGuid()+".png"), new Point(625, 40),"534261723521");
    }


    public static string SaveTextOnImage(string inputFile, string outputFileName, Point point, string VoucherCode)
    {

        //Load the Image to be written on.
        Bitmap bitMapImage = new System.Drawing.Bitmap(inputFile);
        Graphics graphicImage = Graphics.FromImage(bitMapImage);

        //Smooth graphics is nice.
        graphicImage.SmoothingMode = SmoothingMode.AntiAlias;

        //I am drawing a oval around my text.++-
       // graphicImage.DrawArc(new Pen(Color.Red, 3), 90, 235, 150, 50, 0, 360);

        //Write your text.
        if(VoucherCode!=null)
        {
            graphicImage.DrawString("Voucher Code: " + VoucherCode, new Font("Arial", 10, FontStyle.Bold), SystemBrushes.WindowText, point);
        }
       

        //Set the content type
       // Response.ContentType = "image/jpeg";

        //Save the new image to the response output stream.
        bitMapImage.Save(outputFileName, ImageFormat.Jpeg);

        //Clean house.
        graphicImage.Dispose();
        bitMapImage.Dispose();

        return outputFileName;
    }




    [WebMethod]
    public static string SaveQuize(string Q1, string Q2_1, string Q2_2, string Q2_3, string Q2_4, string Q2_5, string Q2_6, string Q3, string Q4, string Q5)
    {
        string error = "";
        try
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            // string steps=!(string.IsNullOrEmpty(step))?step:"1";
            List<SqlParameter> parameters = new List<SqlParameter>();
            string query = string.Empty;
            string ID = string.Empty;


            //Check email exist 
            if (HttpContext.Current.Session[GlobalVariables.SessionVariables.QuizeId] != null)
            {
                query = "update SubscribeUsers  Set Q1=@Q1 ,[Q2_1]=@Q2_1,[Q2_2]=@Q2_2,[Q2_3]=@Q2_3,[Q2_4]=@Q2_4,[Q2_5]=@Q2_5,[Q2_6]=@Q2_6,[Q3]=@Q3,[Q4]=@Q4,[Q5]=@Q5 where [Id]=@Id;select SCOPE_IDENTITY() as ID";
                parameters.Add(new SqlParameter("@Q1", Q1));
                parameters.Add(new SqlParameter("@Q2_1", Q2_1));
                parameters.Add(new SqlParameter("@Q2_2", Q2_2));
                parameters.Add(new SqlParameter("@Q2_3", Q2_3));
                parameters.Add(new SqlParameter("@Q2_4", Q2_4));
                parameters.Add(new SqlParameter("@Q2_5", Q2_5));
                parameters.Add(new SqlParameter("@Q2_6", Q2_6));
                parameters.Add(new SqlParameter("@Q3", Q3));


                parameters.Add(new SqlParameter("@Q4", Q4));
                parameters.Add(new SqlParameter("@Q5", Q5));
                ID = HttpContext.Current.Session[GlobalVariables.SessionVariables.QuizeId].ToString();
                parameters.Add(new SqlParameter("@Id", ID));

            }
            else
            {

                var language = GlobalUtilitiesOperations.GetCookie(GlobalVariables.CookieVariables.LanguageCookie);
                if (string.IsNullOrEmpty(language))
                {
                    language = GlobalVariables.DefaultLanguage;
                }

                //Insert Case
                query = "INSERT INTO SubscribeUsers(Quizstarttime,Quizlang, Q1) VALUES (@Quizstarttime,@Quizlang,@Q1);select SCOPE_IDENTITY() as ID";
                parameters.Add(new SqlParameter("@Quizstarttime", System.DateTime.Now));   //quizstarttime
                parameters.Add(new SqlParameter("@Quizlang", language));
                parameters.Add(new SqlParameter("@Q1", Q1));



            }


            object id = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, query, parameters.ToArray());

            HttpContext.Current.Session[GlobalVariables.SessionVariables.QuizeId] = !string.IsNullOrEmpty(ID) ? ID : id.ToString();


        }
        catch (Exception ex)
        {
            error = ex.Message;
        }
        return error;
    }
    [WebMethod]
    public static string SaveSubscribeEmail(string firstname, string email, string NewsLetter, string lastname, string mobile, string city)
    {
        string message = "";
        bool status = false;

        try
        {
            string query = string.Empty;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string FirstName = firstname;
            string Lastname = lastname;
            string Mobile = mobile;
            string City = city;
            string Email = email;
            DateTime DateOfRegistration = System.DateTime.Now;
            int SourceofRegistration = 1;
            int Active = 1;
            int Id = 1;

            int VoucherStatus = 1;
            DateTime VoucherDate = System.DateTime.Now;
            DateTime RedeemDate = System.DateTime.Now;
            DateTime ReminderDate = System.DateTime.Now.AddDays(7);
            string VoucherCode = "";
            int VoucherID;
            //Check email exist 

            string query1 = "SELECT EMAIL FROM SubscribeUsers where email=@Email";
            List<SqlParameter> parametersForQuery1 = new List<SqlParameter>();
            parametersForQuery1.Add(new SqlParameter("@Email", Email));
            DataSet sdr = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, query1, parametersForQuery1.ToArray());

            if (sdr.Tables.Count > 0 && sdr.Tables[0].Rows.Count > 0)
            {
                message = "This email has been used already.";
            }
            else
            {
                string queryVoucherid = "SELECT max(VoucherID) FROM SubscribeUsers";
                object value = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, queryVoucherid);
                if (!(value is DBNull))
                {
                    VoucherID = Convert.ToInt16(value) + 1;
                }
                else
                {
                    VoucherID = 1;
                }
                string ID = HttpContext.Current.Session[GlobalVariables.SessionVariables.QuizeId].ToString();
                VoucherCode = VoucherDate.ToString("yyyyMMdd") + VoucherID.ToString().PadLeft(5, '0');

                //changes by anuj for send voucher code on voucher image
                var voucherImageName = Guid.NewGuid();
                string voucherImage = Question.SaveTextOnImage(HttpContext.Current.Server.MapPath("~/Image/image.png"), HttpContext.Current.Server.MapPath("~/Image/" + voucherImageName + ".png"), new Point(625, 40), VoucherCode);
                //Insert Case
                List<SqlParameter> parameters = new List<SqlParameter>();
                if (HttpContext.Current.Session[GlobalVariables.SessionVariables.QuizeId] != null)
                {
                    query = "update SubscribeUsers  Set FirstName=@FirstName,Lastname=@Lastname,Mobile=@Mobile,City=@City,Email=@Email,DateOfRegistration=@DateOfRegistration,SourceofRegistration=@SourceofRegistration,Active=@Active,VoucherID=@VoucherID,VoucherStatus=@VoucherStatus,VoucherDate=@VoucherDate,VoucherCode=@VoucherCode,Newsletter=@Newsletter,ReminderDate =@ReminderDate where [Id]=@Id  ;select SCOPE_IDENTITY() as ID";

                    parameters.Add(new SqlParameter("@Id", ID));
                }
                else
                {
                    query = "INSERT INTO SubscribeUsers(FirstName,Lastname,Mobile,City,Email,DateOfRegistration,SourceofRegistration,Active,VoucherID,VoucherStatus,VoucherDate,VoucherCode,Newsletter,ReminderDate,Quizstarttime,Quizlang) VALUES (@FirstName,@Lastname,@Mobile,@City, @Email,@DateOfRegistration,@SourceofRegistration,@Active,@VoucherID,@VoucherStatus,@VoucherDate,@VoucherCode,@Newsletter,@ReminderDate,@Quizstarttime,@Quizlang);select SCOPE_IDENTITY() as ID";
                    parameters.Add(new SqlParameter("@Quizstarttime", System.DateTime.Now));   //quizstarttime
                    var language = GlobalUtilitiesOperations.GetCookie(GlobalVariables.CookieVariables.LanguageCookie);
                    if (string.IsNullOrEmpty(language))
                    {
                        language = GlobalVariables.DefaultLanguage;
                    }
                    parameters.Add(new SqlParameter("@Quizlang", language));
                }


                parameters.Add(new SqlParameter("@FirstName", FirstName));
                parameters.Add(new SqlParameter("@Lastname", Lastname));
                parameters.Add(new SqlParameter("@Mobile", Mobile));
                parameters.Add(new SqlParameter("@City", City));
                parameters.Add(new SqlParameter("@Email", Email));
                parameters.Add(new SqlParameter("@DateOfRegistration", DateOfRegistration));
                parameters.Add(new SqlParameter("@SourceofRegistration", SourceofRegistration));
                parameters.Add(new SqlParameter("@Active", Active));

                parameters.Add(new SqlParameter("@VoucherID", VoucherID));
                parameters.Add(new SqlParameter("@VoucherStatus", VoucherStatus));
                parameters.Add(new SqlParameter("@VoucherDate", VoucherDate));
                parameters.Add(new SqlParameter("@VoucherCode", VoucherCode));
                //parameters.Add(new SqlParameter("@RedeemDate", RedeemDate));

                parameters.Add(new SqlParameter("@ReminderDate", ReminderDate));
                parameters.Add(new SqlParameter("@Newsletter", NewsLetter));
                SqlHelper.ExecuteScalar(connectionString, CommandType.Text, query, parameters.ToArray());

                var EmailSubject = "Your TYREPLUS voucher";
                //Send Email 
                var verifyLink = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/UnSubscribeEmail.aspx/?id=" + ID;
                try
                {
                    var imageLink = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Image/" + voucherImageName + ".png";                  

                    var emailBody = "Hi " + FirstName +
                        ",<br /><br />Thank you for completing the TYREPLUS Quiz. Your submission entitles you to either of the below options* <br/> <img src='" +
                        imageLink + "'><br />  Voucher number: " + VoucherCode + " <br />  " +
                        "<br />Please note this offer is valid until May 31st. Please ensure you have your voucher number with you when you visit the branch. We look forward to welcoming you to <a href='http://tyreplusquiz.com/Contact.aspx'>one of our branches</a> soon. <br />" +
                        "<br /><br /><a href='http://tyreplusquiz.com/Terms.aspx'>*Terms and conditions apply</a> ";      
                    var language = GlobalUtilitiesOperations.GetCookie(GlobalVariables.CookieVariables.LanguageCookie);
                    if (string.IsNullOrEmpty(language))
                    {
                        language = GlobalVariables.DefaultLanguage;
                    }

                    if (language == GlobalVariables.ArabicLanguage)
                    {
                        emailBody = "مرحباً، " + FirstName +
                        ",<br /><br />شكراً على مشاركتك. يحق لك إختيار إحدى هذه الخيارات*<br/> <img src='" +
                        imageLink + "'><br /> " + VoucherCode + " :رقم القسيمة<br />  " +
                        "<br />يرجى الملاحظة أن القسيمة صالحة حتى يوم ٣١ مايو ٢٠١٧. يرجى التأكد من توفر رقم القسيمة عند تقديمها. نتطلع لزيارتك في إحدى فروعنا قريباً.<br />" +
                        "<br /><br /><a href='http://tyreplusquiz.com/Terms.aspx'>*تطبق الشروط و الأحكام</a> ";
                        EmailSubject = "قسيمتك من تاير بلاس";
                    }

                    EmailServices.SendMail(Email, emailBody, EmailSubject);
                    HttpContext.Current.Session[GlobalVariables.SessionVariables.QuizeId] = null;
                }
                catch (Exception ex)
                {


                }
                message = "You have subscribed successfully.";
                status = true;
                //JavaScriptSerializer serialiser = new JavaScriptSerializer();
                //message = serialiser.Serialize(result);

                // return res;
            }
        }
        catch (Exception ex)
        {
            message = ex.Message;
        }
        JavaScriptSerializer serialiser = new JavaScriptSerializer();
        var result = serialiser.Serialize(new { status = status, Message = message });
        return result;
    }
}