$(document).ready(function () {
    // $(".questions_container, .question_progress, .next_button").addClass("hide");
    // $(".form_container").addClass("visible");

        $('#s_facebook').ShareLink({
                title: 'TYREPLUS Quiz',
                text: 'I just took the TYREPLUS quiz and won a great discount for my next visit to any of their 36 UAE branches. Check it out! - http://tyreplusquiz.com/',
                url: 'http://tyreplusquiz.com/'
        });

        $('#s_twitter').ShareLink({
                title: 'TYREPLUS Quiz',
                text: 'I just took the TYREPLUS quiz and won a great discount for my next visit to their UAE branches. Check it out!',
                url: 'http://tyreplusquiz.com/'
        });
    $(".tankyou_social").mouseover(function () {
        $(this).addClass("social_visible");
    });

    $(".menu_button").click(function () {
        $("header").toggleClass("menu_shown");
    });

    function validateAnswer(el) {
        $(el).parent().parent().parent().addClass("answered");
        $(".next_button").addClass("active");
    }
    function validateAnswerNegative(el) {
        $(el).parent().parent().parent().removeClass("answered");
        $(".next_button").removeClass("active");
    }

    if( $("body").hasClass("lang_arabic") ){
  		$(".question_progress p").text("السؤال 1 / 5");
	} else {
  		$(".question_progress p").text("Question 1 / 5")
	}

    $(".next_button").click(function () {

        var el = $(".question_part.visible");
        $(this).parent().addClass("next_link");


        if ($(".question_part.visible").hasClass("answered")) {
            var el_id = parseInt($(el).attr("id").substring($(el).attr("id").length - 1)) + 1;

            if ($("#question_" + el_id).length !== 0) {
                //I need Question 1/5 on page load but just append the el_id/5 output is 1/5 and append add question auto 
                
                if( $("body").hasClass("lang_arabic") ){
              		$(".question_progress p").text(" السؤال " + el_id + " / 5");
				} else {
              		$(".question_progress p").text("Question " + el_id + " / 5")
				}

                $(".question_progress_bar_progress").addClass("q" + el_id);
                $(el).removeClass("visible");
                $("#question_" + el_id).addClass("visible");
                $(".next_button").attr("data-answered", el_id-1);
                console.log($(".next_button").attr("data-answered"));
            } else {
                $(".analys_container").addClass("visible");
                $(".questions_container, .question_progress, .next_button").addClass("hide");
                setTimeout(function () {
                    $(".analys_container").removeClass("visible");
                    $(".form_container").addClass("visible");
                }, 3000);
            }
        } else {
            $(".question_part.visible").addClass("wrong_click");
            if (amount === 4) {
                $(".question_part.visible").removeClass("wrong_click_less");
                $(".question_part.visible").removeClass("wrong_click_more");
            } else if (amount < 4) {
                $(".question_part.visible").removeClass("wrong_click_less");
                $(".question_part.visible").addClass("wrong_click_more");
            } else if (amount > 4) {
                $(".question_part.visible").removeClass("wrong_click_more");
                $(".question_part.visible").addClass("wrong_click_less");
            }
        }

        var Q1 = "";
        var Q2_1 = "", Q2_2 = "", Q2_3 = "", Q2_4 = "", Q2_5 = "", Q2_6 = "", Q3 = "", Q4 = "", Q5 = "", firstname = "", email = "", NewsLetter = "", lastname = "", mobile = "", city = "";
        if ($("#q1_ch1").prop("checked")) {
            // Q1 = $('label[for=q1_ch1]').text();
            Q1 = "1";
            //$("#q1_ch1").parent().;
        }
        if ($("#q1_ch2").prop("checked")) {
            // Q1 = $('label[for=q1_ch2]').text();
            Q1 = "0";
        }
        if ($("#q2_ch1").prop("checked")) {
            // Q2_1 = $('label[for=q2_ch1]').text();
            Q2_1 = "1";
        }
        else {
            Q2_1 = "0";
        }
        if ($("#q2_ch2").prop("checked")) {
            //Q2_2 = $('label[for=q2_ch2]').text();
            Q2_2 = "1";
        }
        else {
            Q2_2 = "0";
        }
        if ($("#q2_ch3").prop("checked")) {
            // Q2_3 = $('label[for=q2_ch3]').text();
            Q2_3 = "1";
        }
        else {
            Q2_3 = "0";

        }
        if ($("#q2_ch4").prop("checked")) {
            //Q2_4 = $('label[for=q2_ch4]').text();
            Q2_4 = "1";
        }
        else {
            Q2_4 = "0";
        }
        if ($("#q2_ch5").prop("checked")) {
            // Q2_5 = $('label[for=q2_ch5]').text();
            Q2_5 = "1";
        }
        else {
            Q2_5 = "0";
        }
        if ($("#q2_ch6").prop("checked")) {
            //Q2_6 = $('label[for=q2_ch6]').text();
            Q2_6 = "1";
        }
        else {
            Q2_6 = "0";
        }

        if ($("#q3_ch1").prop("checked")) {
            //Q3 = $('label[for=q3_ch1]').text();
            Q3 = "1"
        }
        else if ($("#q3_ch2").prop("checked")) {
            // Q3 = $('label[for=q3_ch2]').text();
            Q3 = "2"
        }
        else if ($("#q3_ch3").prop("checked")) {
            // Q3 = $('label[for=q3_ch3]').text();
            Q3 = "3"
        }
        else if ($("#q3_ch4").prop("checked")) {
            //Q3 = $('label[for=q3_ch4]').text();
            Q3 = "4"
        }
        //Q4
        if ($("#q4_ch1").prop("checked")) {
            //Q4 = $('label[for=q4_ch1]').text();
            Q4 = "1";
        }
        else if ($("#q4_ch2").prop("checked")) {
            // Q4 = $('label[for=q4_ch2]').text();
            Q4 = "2";
        }
        else if ($("#q4_ch3").prop("checked")) {
            //Q4 = $('label[for=q4_ch3]').text();
            Q4 = "3";
        }
        else if ($("#q4_ch4").prop("checked")) {
            // Q4 = $('label[for=q4_ch4]').text();
            Q4 = "4";
        }
        //Q5
        if ($("#q5_ch1").prop("checked")) {
            // Q5 = $('label[for=q5_ch1]').text();
            Q5 = "1";
        }
        else if ($("#q5_ch2").prop("checked")) {
            //Q5 = $('label[for=q5_ch2]').text();
            Q5 = "2";
        }
        else if ($("#q5_ch3").prop("checked")) {
            // Q5 = $('label[for=q5_ch3]').text();
            Q5 = "2";
        }
        else if ($("#q5_ch4").prop("checked")) {
            // Q5 = $('label[for=q5_ch4]').text();
            Q5 = "4";
        }
        //"#, #, #, #"
        //alert(Q1 + "\n" + Q2_1 + "\n" + Q2_2 + "\n" + Q2_3 + "\n" + Q2_4 + "\n" + Q2_5 + "\n" + Q2_6);
        SaveData(Q1, Q2_1, Q2_2, Q2_3, Q2_4, Q2_5, Q2_6, Q3, Q4, Q5);
        $(this).removeClass("active");
    });

    $(".single_answer input[type='checkbox']").click(function () {
        $(this).parent().parent().find("input[type='checkbox']").prop("checked", false);
        $(this).prop("checked", true);
    });

    $("#q1_ch1, #q1_ch2").click(function () {
        if ($("#q1_ch2").prop("checked")) {
            $(".buttons_container").removeClass("hidden");
            $(".buttons_container").removeClass("next_link");
        }
        if ($("#q1_ch1").prop("checked")) {
            $(".buttons_container").removeClass("hidden");
            $(".buttons_container").addClass("next_link");
        }
        if ($("#q1_ch1").prop("checked") || $("#q1_ch2").prop("checked")) {
            validateAnswer($("#q1_ch1"));
        } else {
            validateAnswerNegative($("#q1_ch1"));
        }
    });

    var amount;
    $("#q2_ch1, #q2_ch2, #q2_ch3, #q2_ch4, #q2_ch5, #q2_ch6").click(function () {
        amount = 0;
        $(".four_answers input[type='checkbox']").each(function () {
            if ($(this).prop("checked")) {
                amount++;
            }
        });
        console.log(amount);

        if (amount === 4) {
            validateAnswer($("#q2_ch1"));
        } else if (amount < 4) {
            validateAnswerNegative($("#q2_ch1"));
        } else if (amount > 4) {
            validateAnswerNegative($("#q2_ch1"));
        }
    });

    $("#q3_ch1, #q3_ch2, #q3_ch3, #q3_ch4").click(function () {
        if ($("#q3_ch1").prop("checked") || $("#q3_ch2").prop("checked")
			|| $("#q3_ch3").prop("checked") || $("#q3_ch4").prop("checked")) {
            validateAnswer($("#q3_ch1"));
        } else {
            validateAnswerNegative($("#q3_ch1"));
        }
    });

    $("#q4_ch1, #q4_ch2, #q4_ch3, #q4_ch4").click(function () {
        if ($("#q4_ch1").prop("checked") || $("#q4_ch2").prop("checked")) {
            validateAnswer($("#q4_ch1"));
        } else {
            validateAnswerNegative($("#q4_ch1"));
        }
    });

    $("#q5_ch1, #q5_ch2, #q5_ch3, #q5_ch4").click(function () {
        if ($("#q5_ch1").prop("checked") || $("#q5_ch2").prop("checked")
			|| $("#q5_ch3").prop("checked") || $("#q5_ch4").prop("checked")) {
            validateAnswer($("#q5_ch1"));
        } else {
            validateAnswerNegative($("#q5_ch1"));
        }
    });

    $("input").click(function () {
        if (this.id !== "submit_btn") {
            $("#" + this.id).parent(".material_input").addClass('focused')
        };
    });
    $("input").focus(function () {
        if (this.id !== "submit_btn") {
            $("#" + this.id).parent(".material_input").addClass('focused')
        };
    });
    $("input").focusout(function () {
        if ($("#" + this.id).val() == "") {
            $("#" + this.id).parent(".material_input").removeClass("focused");
        }
    });
    $("input").each(function () {
        if ($("#" + this.id).val() != "") {
            $("#" + this.id).parent(".material_input").addClass("focused");
        }
    });

    // custom dropdown
    $(".custom_dropdown").click(function () {
        if (!$(this).find(".dd_options").hasClass("visible")) {
            $(".dd_options").removeClass("visible");
            $(this).find(".dd_options").toggleClass("visible");
            $(".dd_label").addClass("focused");
        } else {
            $(this).find(".dd_options").removeClass("visible");
        }
    });

    var selectedCity = '';
    $(".dd_options li").click(function () {       
        $(this).parent().parent().find(".dd_label_value").text($(this).text());
        selectedCity = $(this).data("id");
        $(this).parent().parent().find("input").val($(this).text());
        $(".dd_label").addClass("focused");
    });


    function SaveData(Q1, Q2_1, Q2_2, Q2_3, Q2_4, Q2_5, Q2_6, Q3, Q4, Q5) {
        if ($(".next_button").hasClass("active")) {

            $.ajax({
                type: "POST",
                url: "Question.aspx/SaveQuize",
                data: "{ Q1 :'" + Q1 + "', Q2_1 : '" + Q2_1 + "', Q2_2 : '" + Q2_2 + "', Q2_3 : '" + Q2_3 + "', Q2_4 : '" + Q2_4 + "', Q2_5 : '" + Q2_5 + "', Q2_6 : '" + Q2_6 + "', Q3 : '" + Q3 + "', Q4 : '" + Q4 + "', Q5 : '" + Q5 + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    //alert(result.d);
                }
            });
        }
        else {
            //alert("no");
        }


    }

    var submit_btnclick = false;


    $("#submit_btn").click(function () {
      
            SaveDataSubcribeDetails();
          
        

    });
    function removeErrors(container) {
        container.removeClass("er1 er2 er3 er4 er5");
    }
    function SaveDataSubcribeDetails() {

        removeErrors($(".error_message"));

        var EmailId = $("#email").val() || '';
        var Username = $("#u_name").val() || '';
        var msg = "";
        var NewsLetter;
        var Terms;
        var lastname = $("#u_last_name").val();
        var mobile = $("#mobile").val();      
        var city = selectedCity;
        var terms = $("#ch1").prop("checked");
        var chknewsletter = document.getElementById("ch2");
        var termCond = document.getElementById("ch1");
        if (termCond.checked) {
            Terms = 1;
        }
        else {
            Terms = 0;
        }
        if (chknewsletter.checked) {
            NewsLetter = 1;
        } else {
            NewsLetter = 0;
        }
        var emailfilter = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,20}))+$/;

        if (Username == "") {
            $(".error_message").addClass("er1");
        }
        else if (lastname == "") {
            $(".error_message").addClass("er2");
        }
        else if (EmailId == "") {
            $(".error_message").addClass("er3");
        }
        else if (!(emailfilter.test(EmailId))) {
            $(".error_message").addClass("er3");
        }
        else if (mobile == "") {
            $(".error_message").addClass("er4");
        }
        else if (!terms) {
            $(".error_message").addClass("er5");
        }
        else if (Terms == 0) {
            $(".error_message").addClass("er5");
        }
        else if (city == "") {
            $(".error_message").addClass("er7");
        }

        else {     
            if(submit_btnclick == false)
            {
                submit_btnclick = true;
                $.ajax({
                    type: "POST",
                    url: "Question.aspx/SaveSubscribeEmail",
                    data: "{ firstname:'" + Username + "', email:'" + EmailId + "', NewsLetter:'" + NewsLetter + "',lastname:'" + lastname + "',mobile:'" + mobile + "',city:'" + city + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        submit_btnclick = false;
                        result = JSON.parse(result.d);
                        if (result.status == true) {
                            window.location.href = "Thank_you.aspx";
                        }
                        else {
                         
                            $(".error_message").addClass("er6");
                        }
                    }
                });
            }
        }

    }

    $(".branch li").click(function () {
        if ($(this).hasClass("active")) {
            $(".branch li").removeClass("active");
        } else {
            $(".branch li").removeClass("active");
            $(this).addClass("active");
        }
    });




    if ($('#v_number').length > 0) {
        // $("#v_number").autocomplete({

        //     minLength: 0,
        //     source: function (request, response) {
        //         $.ajax({
        //             url: "/Admin/DashBoard.aspx/VoucherDetails",
        //             type: "POST",
        //             contentType: "application/json; charset=utf-8",
        //             dataType: "json",
        //             data: "{voucherNo:'" + request.term + "'}",
        //             success: function (data) {
        //                 console.log(data);
        //                 var json = JSON.parse(data.d);

        //                 if (!json.length) {
        //                     var result = [
        //                      {
        //                          VoucherCode: 'No matches found',
        //                          Name: '',
        //                          RegDate: '',
        //                          VoucherTypes: '',
        //                          Branch: '',
        //                          VoucherStatus: ''
        //                      }
        //                     ];
        //                     response(result);
        //                 }
        //                 else {

        //                     response($.map(json, function (item) {

        //                         return item;//{ label: str, value: item.LeadId };
        //                     }));
        //                 }

        //             }
        //         });
        //     },
        //     focus: function (event, ui) {
        //         //$("#txtAutoSearchLead").val(ui.item.FirstName + " " + ui.item.LastName);                  
        //         return false;
        //     },
        //     select: function (event, ui) {
        //         if (ui.item.VoucherCode != "No matches found") {
        //             $('#voucherDetails').show();
        //             $("#v_number").val(ui.item.VoucherCode);

        //             $('#lblName').html(ui.item.Name);
        //             $('#lblRegDate').html(ui.item.RegDate);
        //             $('#lblVoucherType').html(ui.item.VoucherTypes);
        //             $('#lblBranch').html(ui.item.Branch);
        //             $('#lblVoucherNo').html(ui.item.VoucherCode);
                 
                  
                   
        //             $('#lblVoucherStatus').html(ui.item.VoucherStatus);
        //         }
        //         return false;
        //     }
        // })
        //         .autocomplete("instance")._renderItem = function (ul, item) {
        //             var str = "";
        //             if (item.VoucherCode != "No matches found") {
        //                 //str = str + "<strong>" + item.FirstName + " " + item.LastName + " (" + item.LeadState + ")</strong><br>";
        //                 if (item.VoucherCode) {
        //                     str = str + item.VoucherCode + " ";
        //                 }

        //             }
        //             else {
        //                 str = str + "No matches found";
        //             }
        //             return $("<li>")
        //               .append("<div>" + str + "</div>")
        //               .appendTo(ul);
        //         };
    };

    //end here of coding of autocomplete
    //Index page 
    $('#vnumber').keyup(function () {
        ValidVoucher();
    });
    function ValidVoucher() {
        $('.login_form').removeClass('error_invalid');
        var voucherNo = $('#vnumber').val() || '';
        if (voucherNo != '' && voucherNo.length >= 13) {
            $.ajax({
                type: "POST",
                url: "/Admin/Index.aspx/VoucherDetails",
                data: "{ voucherNo:'" + voucherNo + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d == "Valid") {
                        $('.login_form').removeClass('error_invalid');

                    }
                    else {

                        $('.login_form').addClass('error_invalid');

                    }
                }
            });
        }

    };
    $("#Submit_RedeemVoucher").click(function () {

        SaveDataRedeemVoucher();

    });
    function SaveDataRedeemVoucher() {

        var voucherNo = $("#vnumber").val() || '';
        var serviceType = $(".dd_label_value").html() || '';
        if (voucherNo != '' && serviceType != '' && voucherNo.length >= 13) {        
            $.ajax({
                type: "POST",
                url: "/Admin/Index.aspx/RedeemVoucherDetails",
                data: "{ voucherNo:'" + voucherNo + "', serviceType:'" + serviceType + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    //alert(result.d);
                    alert(result.d);
                }
            });
        }
        else {
            $('.login_form').addClass('error_invalid');
        }
    }


});