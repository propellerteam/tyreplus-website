﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Admin_Default" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TYREPLUS Quiz</title>
	<link rel="icon" type="image/png" href="../img/favicon.png">	
    <link href="../css/form-rules.css" rel="stylesheet" />
    <link href="../css/login_form.css" rel="stylesheet" />
    <link href="../css/login_page.css" rel="stylesheet" />
    <link href="../css/contact.css" rel="stylesheet" />
    <link href="../css/main.css" rel="stylesheet" />	
</head>
<body> 

	<div class="content_container content_container_landing">
		<div class="login_form">
			<form runat="server" id="form1" class="form">
				<img src="../img/logo.svg" alt="">
				<p class="error_message"  id="error_message" runat="server">
					<span class="error1">
						<%=ResourceFileManager.Get("dafault_page_login_error","Enter a valid username and/or password.")%>
					</span>
				</p>
				<div class="material_input">
					<label for="txtUserName">Username</label>
					  <asp:TextBox ID="txtUserName" runat="server" ClientIDMode="Static"></asp:TextBox>
				</div>
				<div class="material_input">
					<label for="txtPassword">Password</label>
				 <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" ClientIDMode="Static"></asp:TextBox>
				</div>
<%-- <asp:Button ID="btnSave" runat="server" CssClass="submit_btn" Text="Sign in" OnClick="btnSave_Click" />--%>
                <asp:LinkButton ID="btnSave" runat="server" CssClass="login_submit_btn submit_btn" OnClick="btnSave_Click" Font-Underline="false"><span>Sign in</span></asp:LinkButton>
	<%--<a class="submit_btn" id="submit_btn">Sign in</a>--%>
               <%--<h3> <asp:Label ID="lblMessage" runat="server"  Text="" ClientIDMode="Static"></asp:Label></h3>--%>
			</form>
		</div>
	</div>
	
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>	
    <script src="../js/main.js"></script>
    <script src="../js/socialshare.js"></script>   
	<script type="text/javascript">
		$(".login_submit_btn").click(function(e){
			var u_name = $("#txtUserName").val();
			var password = $("#txtPassword").val();
		
			if( (u_name == "") || (password == "") ){
				$(".error_message").addClass("er1");
			} else {
				$(".error_message").removeClass("er1");
			}
		});
	</script>
</body>
</html>
